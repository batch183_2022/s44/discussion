//fetch() method in Javascript is used to send request in the server and load the received response in the webpages. The request and response is in JSON format.

// Syntax:
	//fetch('url', options)
		// url - this is the url which the request is to be made (endpotin).
		// options - array of properties that contains the HTTP method, body of request, headers.

//Get post data
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())

// .then((data) => console.log(data)) 				//to display the entire content of db

//invoke the showposts
.then((data) => showPosts(data));

// Add post data.	
																//e = event
document.querySelector("#form-add-post").addEventListener("submit", (e)=>{
	// Prevents the page from loading
	e.preventDefault()

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers:{
			"Content-Type":"application/json"
		}
	})
	.then((response)=> response.json())  // chain of response no need semicolon because its .then method
	.then((data)=>{
		console.log(data);
		alert("Successfully Added!");
	})
	
	// resets the state of our input into blanks after submitting a new post.
	document.querySelector("#txt-title").value = null;
	document.querySelector("#txt-body").value =null;

})

// View posts
const showPosts = (posts) => {
	let postEntries = "";

	// We will used forEach() to display each movie inside our mock database.
	posts.forEach((post)=>{
		postEntries += `
			<div id="post-${post.id}">
				<h3 id ="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	// console.log(postEntries);  // to display in console all content https://jsonplaceholder.typicode.com/posts
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit Post Button
const editPost = (id) =>{
	//Contain the value of the title and bocy in a variable.
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	//Pass the id, title, and body of the movie post to be updated in the edit post/form.
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	// To removed the disable property
	document.querySelector("#btn-submit-update").removeAttribute("disabled");
}

//Update post
document.querySelector("#form-edit-post").addEventListener("submit", (e)=>{
	e.preventDefault();
	
	let id = document.querySelector("#txt-edit-id").value
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
		method: "PUT",
		body: JSON.stringify({
			id: id,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers:{
			"Content-Type":"application/json"
		}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert("Successfully updated!");

		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;

		// Add a attribute in a HTML element.
		document.querySelector("#btn-submit-update").setAttribute("disabled", true);
	})
})


//Delete post
// const deletePost = (id) =>{
// 		for(i = 0; i < posts.length; i++){
// 		if(posts[i].id == id){
// 			posts.splice(i,1);
// 			document.querySelector(`#post-${id}`).remove();
// 			alert("Successfully deleted!");
// 			break;
// 		}
// 	}

// }

//Activity Delete a post
const deletePost = (id) => {
	// filter method will save the posts that are not equal to the id parameter.
	posts = posts.filter((post) => {
		if(post.id != id){
			return post;
		}
	})

	console.log(posts);

	// .remove() method removes an element (or node) from the document.
	document.querySelector(`#post-${id}`).remove();
}
